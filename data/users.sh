#!/usr/bin/env bash

#Create user logins and ssh keys
sudo -s
useradd -m -d /home/acastro -s /bin/bash acastro -G wheel
sudo echo 'acastro ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
sudo su - acastro
mkdir /home/acastro/.ssh
echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCmy08wq8xTUSo9iZ51DVt2Uo2JVrLEDspVtQTcWh9v+RGO/cYKrU9KtqF82tna3SYYpTp3+M8xptmCILeMEzvWZKayf1JX0Qz3+OAOUv4cVJRI6UudNYob4L1IsIq314pLcfl0v2Yv46rh46xR473bBDL+UfMJ+PW/WSghnPmXI67loaNnlj0rM1Ul2XKMB7Cn+wPFwWKbdefEm28ljI4Ox/CS9KPM27m+LKLJxDllzwinjH2YQGyutwnUNl/ORgK+EUKRQyzR99s/NZ9k5PyNMVzSjDZ5K3fDWMiOzpGOqLPddZudehYKT/Njq+vGwGFUGyA90C8OmYngfClmHdhr angel.castrobasurto@S05681-MBPR.local > /home/acastro/.ssh/authorized_keys
chown -R acastro:acastro /home/acastro/.ssh
chmod 700 /home/acastro/.ssh
chmod 600 /home/acastro/.ssh/authorized_keys
