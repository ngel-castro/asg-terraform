resource "aws_launch_configuration" "WebServer_Launch_Configuration" {
    name_prefix = "test-WebServer-LaunchConfiguration-"
    image_id = "ami-0b69ea66ff7391e80"
    instance_type = "t2.micro"
    key_name = "angel.castro"
    root_block_device {
        volume_type = "gp2"
        volume_size = 8
        delete_on_termination = true
    }

    user_data = "${data.template_cloudinit_config.WebServer.rendered}"
    lifecycle { create_before_destroy = "true"}    
}

resource "aws_autoscaling_group" "WebServer_ASG" {
    name = "Test-WebServer-ASG"
    desired_capacity = 1
    min_size = 1
    max_size = 1
    health_check_type = "EC2"
    health_check_grace_period = 600
    lifecycle { ignore_changes = ["desired_capacity", "min_size", "max_size"] }    
    metrics_granularity = "1Minute"
    launch_configuration = "${aws_launch_configuration.WebServer_Launch_Configuration.name}"
    availability_zones = ["us-east-1b"]
}