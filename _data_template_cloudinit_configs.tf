data "template_cloudinit_config" "WebServer" {
  gzip = "true"
  base64_encode = "true"

  # get WebServer user_data
  part {
    content = "${data.template_file.WebServer.rendered}"
  }

  # get common user_data
  part {
    content = "${data.template_file.Users.rendered}"
  }
}