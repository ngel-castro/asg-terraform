data "template_file" "WebServer" {
  template = "${file("data/web-server.sh")}"
}

data "template_file" "Users" {
  template = "${file("data/users.sh")}"
}